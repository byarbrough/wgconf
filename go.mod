module gitlab.com/byarbrough/wgconf

go 1.14

require (
	github.com/google/go-cmp v0.4.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/ini.v1 v1.62.0
)
